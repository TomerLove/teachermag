package top.tommyteam.dao;
import java.util.List;
import java.util.Map;

import top.tommyteam.module.Contest;
import top.tommyteam.module.Team;
import top.tommyteam.module.User;


/**
 * 用户DAO接口
 * 
 * @author Administrator
 * 
 */
public interface TeamMapper {
    
    /**
     * 用户查询
     * @param map
     * @return用户集合
     */
    public List<Team> find(Map<String, Object> map);
    
    /**
     * 获取总记录数
     * @param map
     * @return获取的total数
     */
    public Long getTotal(Map<String, Object> map);
    
    /**
     * 修改用户
     * @param user
     * @return影响的记录数
     */
    public int update(Team team);
    
    /**
     * 添加用户
     * @param user
     * @return影响的记录数
     */
    public int add(Team team);
    
    /**
     * 删除用户
     * @param id
     * @return
     */
    public int delete(Integer id);
    
    
}