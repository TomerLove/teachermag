package top.tommyteam.dao;
import java.util.List;
import java.util.Map;

import top.tommyteam.module.Honor;
import top.tommyteam.module.User;


/**
 * 用户DAO接口
 * 
 * @author Administrator
 * 
 */
public interface HonorMapper {
    
    /**
     * 用户查询
     * @param map
     * @return用户集合
     */
    public List<Honor> find(Map<String, Object> map);
    
    /**
     * 获取总记录数
     * @param map
     * @return获取的total数
     */
    public Long getTotal(Map<String, Object> map);
    
    /**
     * 修改用户
     * @param user
     * @return影响的记录数
     */
    public int update(Honor honor);
    
    /**
     * 添加用户
     * @param user
     * @return影响的记录数
     */
    public int add(Honor honor);
    
    /**
     * 删除用户
     * @param id
     * @return
     */
    public int delete(Integer id);
    
    
}