package top.tommyteam.service;
import java.util.List;
import java.util.Map;

import top.tommyteam.module.Honor;
import top.tommyteam.module.User;


/**
 * UserService接口
 * @author Administrator
 *
 */
public interface HonorService {
    
    /**
     * 用户查询
     * @param map
     * @return
     */
    public List<Honor> find(Map<String, Object> map);
    
    /**
     * 获取总记录数
     * @param map
     * @return
     */
    public Long getTotal(Map<String, Object> map);
    
    /**
     * 修改用户
     * @param user
     * @return影响的记录数
     */
    public int update(Honor honor);
    
    /**
     * 添加用户
     * @param user
     * @return影响的记录数
     */
    public int add(Honor honor);
    
    /**
     * 删除用户
     * @param id
     * @return
     */
    public int delete(Integer id);
}