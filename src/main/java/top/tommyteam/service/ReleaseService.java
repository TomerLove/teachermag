package top.tommyteam.service;
import java.util.List;
import java.util.Map;

import top.tommyteam.module.Release;
import top.tommyteam.module.User;


/**
 * UserService接口
 * @author Administrator
 *
 */
public interface ReleaseService {
    
    /**
     * 用户查询
     * @param map
     * @return
     */
    public List<Release> find(Map<String, Object> map);
    
    /**
     * 获取总记录数
     * @param map
     * @return
     */
    public Long getTotal(Map<String, Object> map);
    
    /**
     * 修改用户
     * @param user
     * @return影响的记录数
     */
    public int update(Release release);
    
    /**
     * 添加用户
     * @param user
     * @return影响的记录数
     */
    public int add(Release release);
    
    /**
     * 删除用户
     * @param id
     * @return
     */
    public int delete(Integer id);
}