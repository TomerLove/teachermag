package top.tommyteam.service.impl;


  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.ReleaseMapper;
import top.tommyteam.module.Release;
import top.tommyteam.service.ReleaseService;
 
/**
 * ReleaseService实现类
 * @author Administrator
 *
 */
@Service("releaseService")
public class ReleaseServiceImpl implements ReleaseService{
    
	@Resource
    private ReleaseMapper releaseMapper;
 
    public List<Release> find(Map<String, Object> map) {
        return releaseMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return releaseMapper.getTotal(map);
    }

    public int update(Release release) {
        return releaseMapper.update(release);
    }

    public int add(Release release) {
        return releaseMapper.add(release);
    }

    public int delete(Integer id) {
        return releaseMapper.delete(id);
    }
}