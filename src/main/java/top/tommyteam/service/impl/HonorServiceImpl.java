package top.tommyteam.service.impl;


  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.HonorMapper;
import top.tommyteam.module.Honor;
import top.tommyteam.service.HonorService;
 
/**
 * HonorService实现类
 * @author Administrator
 *
 */
@Service("honorService")
public class HonorServiceImpl implements HonorService{
    
	@Resource
    private HonorMapper honorMapper;
 
    public List<Honor> find(Map<String, Object> map) {
        return honorMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return honorMapper.getTotal(map);
    }

    public int update(Honor honor) {
        return honorMapper.update(honor);
    }

    public int add(Honor honor) {
        return honorMapper.add(honor);
    }

    public int delete(Integer id) {
        return honorMapper.delete(id);
    }
}