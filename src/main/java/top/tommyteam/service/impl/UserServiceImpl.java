package top.tommyteam.service.impl;


  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.UserMapper;
import top.tommyteam.module.User;
import top.tommyteam.service.UserService;
 
/**
 * UserService实现类
 * @author Administrator
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService{
    
	@Resource
    private UserMapper userMapper;
    
    public User login(User user) {
        return userMapper.login(user);
    }
    
    public List<User> find(Map<String, Object> map) {
        return userMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return userMapper.getTotal(map);
    }

    public int update(User user) {
        return userMapper.update(user);
    }

    public int add(User user) {
        return userMapper.add(user);
    }

    public int delete(Integer id) {
        return userMapper.delete(id);
    }
}