package top.tommyteam.service.impl;



  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.UserTeamMapper;
import top.tommyteam.module.UserTeam;
import top.tommyteam.service.UserTeamService;
 
/**
 * TeamService实现类
 * @author Administrator
 *
 */
@Service("userTeamService")
public class UserTeamServiceImpl implements UserTeamService{
    
	@Resource
    private UserTeamMapper userTeamMapper;
 
    public List<UserTeam> find(Map<String, Object> map) {
        return userTeamMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return userTeamMapper.getTotal(map);
    }

    public int update(UserTeam userTeam) {
        return userTeamMapper.update(userTeam);
    }

    public int add(UserTeam userTeam) {
        return userTeamMapper.add(userTeam);
    }

    public int delete(Integer id) {
        return userTeamMapper.delete(id);
    }
}