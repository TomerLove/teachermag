package top.tommyteam.service.impl;


  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.TeamMapper;
import top.tommyteam.module.Team;
import top.tommyteam.service.TeamService;
 
/**
 * TeamService实现类
 * @author Administrator
 *
 */
@Service("teamService")
public class TeamServiceImpl implements TeamService{
    
	@Resource
    private TeamMapper teamMapper;
 
    public List<Team> find(Map<String, Object> map) {
        return teamMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return teamMapper.getTotal(map);
    }

    public int update(Team team) {
        return teamMapper.update(team);
    }

    public int add(Team team) {
        return teamMapper.add(team);
    }

    public int delete(Integer id) {
        return teamMapper.delete(id);
    }
}