package top.tommyteam.service.impl;


  import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import top.tommyteam.dao.ContestMapper;
import top.tommyteam.module.Contest;
import top.tommyteam.service.ContestService;
 
/**
 * ContestService实现类
 * @author Administrator
 *
 */
@Service("contestService")
public class ContestServiceImpl implements ContestService{
    
	@Resource
    private ContestMapper contestMapper;
 
    public List<Contest> find(Map<String, Object> map) {
        return contestMapper.find(map);
    }
    
    public Long getTotal(Map<String, Object> map) {
        return contestMapper.getTotal(map);
    }

    public int update(Contest contest) {
        return contestMapper.update(contest);
    }

    public int add(Contest contest) {
        return contestMapper.add(contest);
    }

    public int delete(Integer id) {
        return contestMapper.delete(id);
    }

	public List<Contest> statcontest(Map<String, Object> map) {
		return contestMapper.statcontest(map);
	}
}