package top.tommyteam.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.json.JsonString;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import top.tommyteam.module.Contest;
import top.tommyteam.module.User;
import top.tommyteam.service.ContestService;
import top.tommyteam.service.UserService;
import top.tommyteam.util.PageBean;
import top.tommyteam.util.ResponseUtil;
import top.tommyteam.util.StringUtil;
 
/**
 * 用户Controller层
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/contest")
public class ContestController {
    
	@Resource
    private ContestService contestService;
    
    /**
     * 删除用户
     * @param ids
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam(value="ids") String ids,HttpServletResponse res) throws Exception{
        String[] idStr = ids.split(",");
        JSONObject jsonObject = new JSONObject();
        for (String id : idStr) {
        	contestService.delete(Integer.parseInt(id));
        }
        jsonObject.put("success", true);
        ResponseUtil.write(res, jsonObject);
        return null;
    }
    
    /**
     * 添加或者修改
     * @param user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    public String save(Contest contest,HttpServletResponse res,HttpServletRequest req) throws Exception{
        JSONObject jsonObject = new JSONObject();
        //操作记录条数，初始化为0
        int resultTotal = 0;
        if (contest.getId() == null) {
            resultTotal = contestService.add(contest);
        }else{
            resultTotal = contestService.update(contest);
        }
        if(resultTotal > 0){   //说明修改或添加成功
            jsonObject.put("success", true);
        }else{
            jsonObject.put("success", false);
        }
        ResponseUtil.write(res, jsonObject);
        return null;
    }

    
    /**
     * 用户分页查询
     * @param page
     * @param rows
     * @param s_user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/listAll")
    public String list(@RequestParam(value="page",required=false) String page,@RequestParam(value="rows",required=false) String rows,Contest s_contest,HttpServletResponse res) throws Exception{
        PageBean pageBean=new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("contestno", StringUtil.formatLike(s_contest.getContestno()));
        map.put("start", pageBean.getStart());
        map.put("size", pageBean.getPageSize());
        List<Contest> userList=contestService.find(map);
        Long total=contestService.getTotal(map);
        JSONObject result=new JSONObject();
        JSONArray jsonArray=JSONArray.fromObject(userList);
        result.put("rows", jsonArray);
        result.put("total", total);
        ResponseUtil.write(res, result);
        return null;
    }
    
    /**
     * 用户分页查询
     * @param page
     * @param rows
     * @param s_user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/statcontest")
    public String statcontest(Contest s_contest,HttpServletResponse res) throws Exception{
        //{"legend":["1","2","3","4","5"],"name":"销量","data":[{"value":"335", "name":"1"},{"value":"310", "name":"2"},{"value":"234", "name":"3"},{"value":"135", "name":"4"},{"value":"548", "name":"5"}]}
        Map<String,Object> map=new HashMap<String,Object>();
        String json = "";
        List<Contest> userList=contestService.statcontest(map);
		json += "{\"legend\":[";
        for (int i = 0; i < userList.size(); i++) {
        	if (i < userList.size() - 1)
        		json += "\""+((Contest)userList.get(i)).getContestlevel()+"\",";
        	else
        		json += "\""+((Contest)userList.get(i)).getContestlevel()+"\"";
		}
		json += "],";
		json += "\"name\":\"销量\",";
		json += "\"data\":[";
        for (int i = 0; i < userList.size(); i++) {
        	if (i < userList.size() - 1)
        		json += "{\"value\":"+((Contest)userList.get(i)).getContestname()+", \"name\":\""+((Contest)userList.get(i)).getContestlevel()+"\"},";
        	else
        		json += "{\"value\":"+((Contest)userList.get(i)).getContestname()+", \"name\":\""+((Contest)userList.get(i)).getContestlevel()+"\"}";
    	}
        json += "]}";
        JSONObject result = JSONObject.fromObject(json);
        System.out.println("比赛等级统计结果："+result);
        ResponseUtil.write(res, result);
        return null;
    }
    
}