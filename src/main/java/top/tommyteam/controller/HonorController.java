package top.tommyteam.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import top.tommyteam.module.Honor;
import top.tommyteam.module.User;
import top.tommyteam.service.HonorService;
import top.tommyteam.service.UserService;
import top.tommyteam.util.PageBean;
import top.tommyteam.util.ResponseUtil;
import top.tommyteam.util.StringUtil;
 
/**
 * 用户Controller层
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/honor")
public class HonorController {
    
	@Resource
    private HonorService honorService;
    
    /**
     * 删除用户
     * @param ids
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam(value="ids") String ids,HttpServletResponse res,HttpServletRequest req) throws Exception{
        JSONObject jsonObject = new JSONObject();
    	if(!"辅导员".equals(((User)req.getSession().getAttribute("currentUser")).getType())){
            jsonObject.put("success", false);
            ResponseUtil.write(res, jsonObject);
            return null;
    	}
        String[] idStr = ids.split(",");
        for (String id : idStr) {
        	honorService.delete(Integer.parseInt(id));
        }
        jsonObject.put("success", true);
        ResponseUtil.write(res, jsonObject);
        return null;
    }
    
    /**
     * 添加或者修改
     * @param user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    public String save(Honor honor,HttpServletResponse res,HttpServletRequest req) throws Exception{
        JSONObject jsonObject = new JSONObject();
    	if(!"辅导员".equals(((User)req.getSession().getAttribute("currentUser")).getType())){
            jsonObject.put("success", false);
            ResponseUtil.write(res, jsonObject);
            return null;
    	}
        //操作记录条数，初始化为0
        int resultTotal = 0;
        if (honor.getId() == null) {
            resultTotal = honorService.add(honor);
        }else{
            resultTotal = honorService.update(honor);
        }
        if(resultTotal > 0){   //说明修改或添加成功
            jsonObject.put("success", true);
        }else{
            jsonObject.put("success", false);
        }
        ResponseUtil.write(res, jsonObject);
        return null;
    }
    
    
    /**
     * 用户分页查询
     * @param page
     * @param rows
     * @param s_user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/listAll")
    public String list(@RequestParam(value="page",required=false) String page,@RequestParam(value="rows",required=false) String rows,Honor s_honor,HttpServletResponse res) throws Exception{
        PageBean pageBean=new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("winnername", StringUtil.formatLike(s_honor.getWinnername()));
        map.put("contestno", StringUtil.formatLike(s_honor.getContestno()));
        map.put("counselor", StringUtil.formatLike(s_honor.getCounselor()));
        map.put("teamno", StringUtil.formatLike(s_honor.getTeamno()));
        map.put("start", pageBean.getStart());
        map.put("size", pageBean.getPageSize());
        List<Honor> userList=honorService.find(map);
        Long total=honorService.getTotal(map);
        JSONObject result=new JSONObject();
        JSONArray jsonArray=JSONArray.fromObject(userList);
        result.put("rows", jsonArray);
        result.put("total", total);
        ResponseUtil.write(res, result);
        return null;
    }
    
}