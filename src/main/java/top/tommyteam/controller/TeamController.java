package top.tommyteam.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import top.tommyteam.module.Team;
import top.tommyteam.module.User;
import top.tommyteam.service.TeamService;
import top.tommyteam.service.UserService;
import top.tommyteam.util.PageBean;
import top.tommyteam.util.ResponseUtil;
import top.tommyteam.util.StringUtil;
 
/**
 * 用户Controller层
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/team")
public class TeamController {
    
	@Resource
    private TeamService teamService;
    
    /**
     * 删除用户
     * @param ids
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam(value="ids") String ids,HttpServletResponse res) throws Exception{
        String[] idStr = ids.split(",");
        JSONObject jsonObject = new JSONObject();
        for (String id : idStr) {
        	teamService.delete(Integer.parseInt(id));
        }
        jsonObject.put("success", true);
        ResponseUtil.write(res, jsonObject);
        return null;
    }
    
    /**
     * 添加或者修改
     * @param user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/save")
    public String save(Team team,HttpServletResponse res) throws Exception{
        //操作记录条数，初始化为0
        int resultTotal = 0;
        if (team.getId() == null) {
            resultTotal = teamService.add(team);
        }else{
            resultTotal = teamService.update(team);
        }
        JSONObject jsonObject = new JSONObject();
        if(resultTotal > 0){   //说明修改或添加成功
            jsonObject.put("success", true);
        }else{
            jsonObject.put("success", false);
        }
        ResponseUtil.write(res, jsonObject);
        return null;
    }
    
    
    /**
     * 用户分页查询
     * @param page
     * @param rows
     * @param s_user
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping("/listAll")
    public String list(@RequestParam(value="page",required=false) String page,@RequestParam(value="rows",required=false) String rows,Team s_team,HttpServletResponse res) throws Exception{
        PageBean pageBean=new PageBean(Integer.parseInt(page),Integer.parseInt(rows));
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("teamno", StringUtil.formatLike(s_team.getTeamno()));
        map.put("start", pageBean.getStart());
        map.put("size", pageBean.getPageSize());
        List<Team> userList=teamService.find(map);
        Long total=teamService.getTotal(map);
        JSONObject result=new JSONObject();
        JSONArray jsonArray=JSONArray.fromObject(userList);
        result.put("rows", jsonArray);
        result.put("total", total);
        ResponseUtil.write(res, result);
        return null;
    }
    
}