package top.tommyteam.module;

public class Team {
    private Integer id;

    private String teamno;

    private String teamname;

    private String teamleader;

    private String counselor;
    
    private String members;

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTeamno() {
		return teamno;
	}

	public void setTeamno(String teamno) {
		this.teamno = teamno;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public String getTeamleader() {
		return teamleader;
	}

	public void setTeamleader(String teamleader) {
		this.teamleader = teamleader;
	}

	public String getCounselor() {
		return counselor;
	}

	public void setCounselor(String counselor) {
		this.counselor = counselor;
	}
 

 
}