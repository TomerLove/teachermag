package top.tommyteam.module;

public class UserTeam {
    private Integer id;
    private String userid;
    private String teamno;
    private String userduty;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getTeamno() {
		return teamno;
	}
	public void setTeamno(String teamno) {
		this.teamno = teamno;
	}
	public String getUserduty() {
		return userduty;
	}
	public void setUserduty(String userduty) {
		this.userduty = userduty;
	}
}