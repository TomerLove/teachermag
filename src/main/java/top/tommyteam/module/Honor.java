package top.tommyteam.module;

public class Honor {
    private Integer id;

    private String winnertype;

    private String winnername;

    private String teamno;

    private String counselor;

    private String ranking;
    
    private String contestno;

	public String getContestno() {
		return contestno;
	}

	public void setContestno(String contestno) {
		this.contestno = contestno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWinnertype() {
		return winnertype;
	}

	public void setWinnertype(String winnertype) {
		this.winnertype = winnertype;
	}

	public String getWinnername() {
		return winnername;
	}

	public void setWinnername(String winnername) {
		this.winnername = winnername;
	}

	public String getTeamno() {
		return teamno;
	}

	public void setTeamno(String teamno) {
		this.teamno = teamno;
	}

	public String getCounselor() {
		return counselor;
	}

	public void setCounselor(String counselor) {
		this.counselor = counselor;
	}

	public String getRanking() {
		return ranking;
	}

	public void setRanking(String ranking) {
		this.ranking = ranking;
	}

 


}