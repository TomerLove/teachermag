package top.tommyteam.module;

public class Release {
    private Integer id;

    private String releaseno;

    private String releasetitle;

    private String releasecontent;

    private String releaseowner;

    private String releasedate;
    
    private String contestno;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReleaseno() {
		return releaseno;
	}

	public void setReleaseno(String releaseno) {
		this.releaseno = releaseno;
	}

	public String getReleasetitle() {
		return releasetitle;
	}

	public void setReleasetitle(String releasetitle) {
		this.releasetitle = releasetitle;
	}

	public String getReleasecontent() {
		return releasecontent;
	}

	public void setReleasecontent(String releasecontent) {
		this.releasecontent = releasecontent;
	}

	public String getReleaseowner() {
		return releaseowner;
	}

	public void setReleaseowner(String releaseowner) {
		this.releaseowner = releaseowner;
	}

	public String getReleasedate() {
		return releasedate;
	}

	public void setReleasedate(String releasedate) {
		this.releasedate = releasedate;
	}

	public String getContestno() {
		return contestno;
	}

	public void setContestno(String contestno) {
		this.contestno = contestno;
	}

 
}