package top.tommyteam.module;

public class Contest {
    private Integer id;

    private String contestno;

    private String contestname;

    private String contestlevel;

    private String contestaddress;

    private String contestorg;

 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContestno() {
		return contestno;
	}

	public void setContestno(String contestno) {
		this.contestno = contestno;
	}

	public String getContestname() {
		return contestname;
	}

	public void setContestname(String contestname) {
		this.contestname = contestname;
	}

	public String getContestlevel() {
		return contestlevel;
	}

	public void setContestlevel(String contestlevel) {
		this.contestlevel = contestlevel;
	}

	public String getContestaddress() {
		return contestaddress;
	}

	public void setContestaddress(String contestaddress) {
		this.contestaddress = contestaddress;
	}

	public String getContestorg() {
		return contestorg;
	}

	public void setContestorg(String contestorg) {
		this.contestorg = contestorg;
	}

}