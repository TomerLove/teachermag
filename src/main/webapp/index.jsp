<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<html>
    <head>
        <meta charset="utf-8" />
        <title>学员大赛管理系统</title>
        
        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css" />
        
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>

		<div id="formContainer">
			<form id="login" method="post" action="./user/login">
				<a href="#" id="flipToRecover" class="flipLink">Forgot?</a>
				<input type="text" name="loginEmail" id="loginEmail" value="admin" />
				<input type="password" name="loginPass" id="loginPass" value="admin" />
				<input type="submit" name="submit" value="登录" />
			</form>
			<form id="recover" method="post" action="./">
				<a href="#" id="flipToLogin" class="flipLink">Forgot?</a>
				<input type="text" name="recoverEmail" id="recoverEmail" value="Email" />
				<input type="submit" name="submit" value="Recover" />
			</form>
		</div>

        <footer>
	        <!-- <h2><i>Tutorial:</i> Apple-like Login Form With CSS 3D Transforms</h2>
            <a class="tzine" href="http://tutorialzine.com/2012/02/apple-like-login-form/">Head on to <i>Tutorial<b>zine</b></i> to download this example</a> -->
        </footer>
        
        <!-- JavaScript includes -->
		<script src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>

    </body>
</html>


