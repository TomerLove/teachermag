<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<title>学员大赛管理系统</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/demo/demo.css"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
    var url;
    function searchUser() {
        $("#dg").datagrid('load', {
            "winnername" : $("#s_winnername").val(),
	        "teamno" : $("#s_teamno").val(),
	        "contestno" : $("#s_contestno").val(),
	        "counselor" : $("#s_counselor").val()
        });
    }
    function openUserAddDialog() {
        $("#dlg").dialog("open").dialog("setTitle", "添加信息");
        url = "${pageContext.request.contextPath}/honor/save";
    }

    function openUserModifyDialog() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length != 1) {
            $.messager.alert("系统提示", "请选择一条要编辑的数据！");
            return;
        }
        var row = selectedRows[0];
        $("#dlg").dialog("open").dialog("setTitle", "编辑信息");
        $("#fm").form("load", row);
        url = "${pageContext.request.contextPath}/honor/save.do?id=" + row.id;
    }
    
    function saveUser() {
        $("#fm").form("submit", {
            url : url,
            onSubmit : function() {
                
                return $(this).form("validate");
            },
            success : function(result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
                } else {
                    $.messager.alert("系统提示", "保存失败！");
                    return;
                }
            }
        });
    }

    function resetValue() {
        $("#winnertype").val("");
        $("#winnername").val("");
        $("#teamno").val("");
        $("#contestno").val("");
        $("#counselor").val("");
        $("#ranking").val("");
    }

    function closeUserDialog() {
        $("#dlg").dialog("close");
        resetValue();
    }

    function deleteUser() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length == 0) {
            $.messager.alert("系统提示", "请选择要删除的数据！");
            return;
        }
        var strIds = [];
        for ( var i = 0; i < selectedRows.length; i++) {
            strIds.push(selectedRows[i].id);
        }
        var ids = strIds.join(",");
        $.messager.confirm("系统提示", "您确定要删除这<font color=red>"
                + selectedRows.length + "</font>条数据吗？", function(r) {
            if (r) {
                $.post("${pageContext.request.contextPath}/honor/delete.do", {
                    ids : ids
                }, function(result) {
                    if (result.success) {
                        $.messager.alert("系统提示", "数据已成功删除！");
                        $("#dg").datagrid("reload");
                    } else {
                        $.messager.alert("系统提示", "数据删除失败，请联系系统管理员！");
                    }
                }, "json");
            }
        });
    }
    function passwordFormatter(value,rowData,rowIndex){
		return '******';
	} 
</script>
</head>

<body style="margin: 1px">

    <table id="dg" title="大赛管理" class="easyui-datagrid" fitColumns="true"
        pagination="true" rownumbers="true"
        url="${pageContext.request.contextPath}/honor/listAll.do" fit="true"
        toolbar="#tb">
        <thead>
            <tr>
				<th field="id" checkbox="true" align="center"></th>
				<th data-options="field:'winnertype',editor:'textbox'">类别</th>
				<th data-options="field:'winnername',editor:'textbox'">获奖者</th>
				<th data-options="field:'teamno',editor:'textbox'">获奖团队</th>
				<th data-options="field:'contestno',editor:'textbox'">大赛名称</th>
				<th data-options="field:'counselor',editor:'textbox'">指导教师</th>
				<th data-options="field:'ranking',editor:'textbox'">名次</th>
            </tr>
        </thead>
    </table>
    <div id="tb">
        <a href="javascript:openUserAddDialog()" class="easyui-linkbutton"
            iconCls="icon-add" plain="true">添加</a> <a
            href="javascript:openUserModifyDialog()" class="easyui-linkbutton"
            iconCls="icon-edit" plain="true">修改</a> <a
            href="javascript:deleteUser()" class="easyui-linkbutton"
            iconCls="icon-remove" plain="true">删除</a>
        <div>
                &nbsp;获胜者：&nbsp;<input type="text" id="s_winnername" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> 
                &nbsp;获胜团队：&nbsp;<input type="text" id="s_winnername" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> 
                &nbsp;大赛名称：&nbsp;<input type="text" id="s_teamno" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> 
                &nbsp;指导教师：&nbsp;<input type="text" id="s_counselor" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> 
                
                <a
                href="javascript:searchUser()" class="easyui-linkbutton"
                iconCls="icon-search" plain="true">查询</a>
        </div>

        <div id="dlg-buttons">
            <a href="javascript:saveUser()" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
                class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>

        <div id="dlg" class="easyui-dialog"
            style="width: 730px;height:280px;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <form method="post" id="fm">
                <table cellspacing="8px;">
                    <tr>
                        <td>类别：</td>
                        <td>
                            <select name="winnertype" class="easyui-combobox"
                            id="winnertype" style="width: 154px;" editable="false"
                            panelHeight="auto">
                                <option value="个人">个人</option>
                                <option value="团队">团队</option>
                        	</select> &nbsp;<span style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>获奖者：</td>
                        <td><input type="text" id="winnername" name="winnername"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>获奖团队：</td>
                        <td><input type="text" id="teamno" name="teamno"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>大赛名称：</td>
                        <td><input type="text" id="contestno" name="contestno" class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>指导教师：</td>
                        <td><input type="text" id="counselor" name="counselor"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>名次：</td>
                        <td><input type="text" id="ranking" name="ranking"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
</body>
</html>