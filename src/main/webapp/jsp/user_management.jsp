<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<title>学员大赛管理系统</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/demo/demo.css"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
    var url;
    function searchUser() {
        $("#dg").datagrid('load', {
            "username" : $("#s_userName").val()
        });
    }
    function openUserAddDialog() {
        $("#dlg").dialog("open").dialog("setTitle", "添加用户信息");
        url = "${pageContext.request.contextPath}/user/save";
    }

    function openUserModifyDialog() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length != 1) {
            $.messager.alert("系统提示", "请选择一条要编辑的数据！");
            return;
        }
        var row = selectedRows[0];
        $("#dlg").dialog("open").dialog("setTitle", "编辑用户信息");
        $("#fm").form("load", row);
        url = "${pageContext.request.contextPath}/user/save.do?id=" + row.id;
    }
    
    function saveUser() {
        $("#fm").form("submit", {
            url : url,
            onSubmit : function() {
                
                return $(this).form("validate");
            },
            success : function(result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
                } else {
                    $.messager.alert("系统提示", "保存失败！");
                    return;
                }
            }
        });
    }

    function resetValue() {
        $("#username").val("");
        $("#password").val("");
        $("#type").val("");
        $("#realname").val("");
        $("#birthday").val("");
        $("#sex").val("");
        $("#userclass").val("");
        $("#collage").val("");
        $("#remark").val("");
    }

    function closeUserDialog() {
        $("#dlg").dialog("close");
        resetValue();
    }

    function deleteUser() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length == 0) {
            $.messager.alert("系统提示", "请选择要删除的数据！");
            return;
        }
        var strIds = [];
        for ( var i = 0; i < selectedRows.length; i++) {
            strIds.push(selectedRows[i].id);
        }
        var ids = strIds.join(",");
        $.messager.confirm("系统提示", "您确定要删除这<font color=red>"
                + selectedRows.length + "</font>条数据吗？", function(r) {
            if (r) {
                $.post("${pageContext.request.contextPath}/user/delete.do", {
                    ids : ids
                }, function(result) {
                    if (result.success) {
                        $.messager.alert("系统提示", "数据已成功删除！");
                        $("#dg").datagrid("reload");
                    } else {
                        $.messager.alert("系统提示", "数据删除失败，请联系系统管理员！");
                    }
                }, "json");
            }
        });
    }
    function passwordFormatter(value,rowData,rowIndex){
		return '******';
	} 
</script>
</head>

<body style="margin: 1px">

    <table id="dg" title="用户管理" class="easyui-datagrid" fitColumns="true"
        pagination="true" rownumbers="true"
        url="${pageContext.request.contextPath}/user/listAll.do" fit="true"
        toolbar="#tb">
        <thead>
            <tr>
				<th field="id" checkbox="true" align="center"></th>
				<th data-options="field:'username',editor:'textbox'">用户名</th>
				<th data-options="field:'password',editor:'textbox',formatter:passwordFormatter">密码</th>
				<th data-options="field:'type',editor:'textbox'">用户类型</th>
				<th data-options="field:'realname',editor:'textbox'">真实姓名</th>
				<th data-options="field:'birthday',editor:'textbox'">生日</th>
				<th data-options="field:'sex',editor:'textbox'">性别</th>
				<th data-options="field:'userclass',editor:'textbox'">班级</th>
				<th data-options="field:'collage',editor:'textbox'">学院</th>
				<th data-options="field:'remark',editor:'textbox'">备注</th>
            </tr>
        </thead>
    </table>
    <div id="tb">
        <a href="javascript:openUserAddDialog()" class="easyui-linkbutton"
            iconCls="icon-add" plain="true">添加</a> <a
            href="javascript:openUserModifyDialog()" class="easyui-linkbutton"
            iconCls="icon-edit" plain="true">修改</a> <a
            href="javascript:deleteUser()" class="easyui-linkbutton"
            iconCls="icon-remove" plain="true">删除</a>
        <div>
            &nbsp;用户名：&nbsp;<input type="text" id="s_userName" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> <a
                href="javascript:searchUser()" class="easyui-linkbutton"
                iconCls="icon-search" plain="true">查询</a>
        </div>

        <div id="dlg-buttons">
            <a href="javascript:saveUser()" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
                class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>

        <div id="dlg" class="easyui-dialog"
            style="width: 730px;height:280px;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <form method="post" id="fm">
                <table cellspacing="8px;">
                    <tr>
                        <td>用户名：</td>
                        <td><input type="text" id="username" name="username"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>密码：</td>
                        <td><input type="password" id="password" name="password"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>真实姓名：</td>
                        <td><input type="text" id="realname" name="realname"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>用户类型：</td>
                        <td>
                            <select name="type" class="easyui-combobox"
                            id="type" style="width: 154px;" editable="false"
                            panelHeight="auto">
                                <option value="系统管理员">系统管理员</option>
                                <option value="学生">学生</option>
                                <option value="团总支负责人">团总支负责人</option>
                                <option value="专业教师">专业教师</option>
                                <option value="辅导员">辅导员</option>
                        	</select> &nbsp;<span style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>生日：</td>
                        <td><input type="text" id="birthday" name="birthday"
                            class="easyui-datebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>性别：</td>
                        <td><select name="sex" class="easyui-combobox"
                            id="sex" style="width: 154px;" 
                            panelHeight="auto">
                                <option value="">请选择角色</option>
                                <option value="男">男</option> 
                                <option value="女">女</option> 
                        </select> &nbsp;<span style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>班级：</td>
                        <td><input type="text" id="userclass" name="userclass"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>学院：</td>
                        <td><input type="text" id="collage" name="collage"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>备注：</td>
                        <td><input type="text" id="remark" name="remark"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td> </td>
                        <td> 
                        </td>
                    </tr>
                </table>
            </form>
        </div>
</body>
</html>