<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<title>学员大赛管理系统</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/demo/demo.css"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
    var url;
    function searchUser() {
        $("#dg").datagrid('load', {
            "teamname" : $("#s_userName").val()
        });
    }
    function openUserAddDialog() {
        $("#dlg").dialog("open").dialog("setTitle", "添加信息");
        url = "${pageContext.request.contextPath}/team/save";
    }

    function openUserModifyDialog() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length != 1) {
            $.messager.alert("系统提示", "请选择一条要编辑的数据！");
            return;
        }
        var row = selectedRows[0];
        $("#dlg").dialog("open").dialog("setTitle", "编辑信息");
        $("#fm").form("load", row);
        url = "${pageContext.request.contextPath}/team/save.do?id=" + row.id;
    }
    
    function saveUser() {
        $("#fm").form("submit", {
            url : url,
            onSubmit : function() {
                
                return $(this).form("validate");
            },
            success : function(result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
                } else {
                    $.messager.alert("系统提示", "保存失败！");
                    return;
                }
            }
        });
    }

    function resetValue() {
        $("#teamno").val("");
        $("#teamname").val("");
        $("#teamleader").val("");
        $("#counselor").val("");
        $("#members").val("");
    }

    function closeUserDialog() {
        $("#dlg").dialog("close");
        resetValue();
    }

    function deleteUser() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length == 0) {
            $.messager.alert("系统提示", "请选择要删除的数据！");
            return;
        }
        var strIds = [];
        for ( var i = 0; i < selectedRows.length; i++) {
            strIds.push(selectedRows[i].id);
        }
        var ids = strIds.join(",");
        $.messager.confirm("系统提示", "您确定要删除这<font color=red>"
                + selectedRows.length + "</font>条数据吗？", function(r) {
            if (r) {
                $.post("${pageContext.request.contextPath}/team/delete.do", {
                    ids : ids
                }, function(result) {
                    if (result.success) {
                        $.messager.alert("系统提示", "数据已成功删除！");
                        $("#dg").datagrid("reload");
                    } else {
                        $.messager.alert("系统提示", "数据删除失败，请联系系统管理员！");
                    }
                }, "json");
            }
        });
    }
    function passwordFormatter(value,rowData,rowIndex){
		return '******';
	} 
</script>
</head>

<body style="margin: 1px">

    <table id="dg" title="团队管理" class="easyui-datagrid" fitColumns="true"
        pagination="true" rownumbers="true"
        url="${pageContext.request.contextPath}/team/listAll.do" fit="true"
        toolbar="#tb">
        <thead>
            <tr>
				<th field="id" checkbox="true" align="center"></th>
				<th data-options="field:'teamno',editor:'textbox'">团队编号</th>
				<th data-options="field:'teamname',editor:'textbox'">团队名称</th>
				<th data-options="field:'teamleader',editor:'textbox'">组长</th>
				<th data-options="field:'counselor',editor:'textbox'">指导教师</th>
				<th data-options="field:'members',editor:'textbox'">成员</th>
            </tr>
        </thead>
    </table>
    <div id="tb">
        <a href="javascript:openUserAddDialog()" class="easyui-linkbutton"
            iconCls="icon-add" plain="true">添加</a> <a
            href="javascript:openUserModifyDialog()" class="easyui-linkbutton"
            iconCls="icon-edit" plain="true">修改</a> <a
            href="javascript:deleteUser()" class="easyui-linkbutton"
            iconCls="icon-remove" plain="true">删除</a>
        <div>
            &nbsp;团队名称：&nbsp;<input type="text" id="s_userName" size="20"
                onkeydown="if(event.keyCode == 13)searchUser()" /> <a
                href="javascript:searchUser()" class="easyui-linkbutton"
                iconCls="icon-search" plain="true">查询</a>
        </div>

        <div id="dlg-buttons">
            <a href="javascript:saveUser()" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
                class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>

        <div id="dlg" class="easyui-dialog"
            style="width:100%;height:100%;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <!-- style="width:930px;height:480px;padding:10px 10px;" -->
            <form method="post" id="fm">
                <table cellspacing="8px;">
                    <tr>
                        <td>团队编号：</td>
                        <td><input type="text" id="teamno" name="teamno"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>团队名称：</td>
                        <td><input type="text" id="teamname" name="teamname"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>组长：</td>
                        <td><input type="text" id="teamleader" name="teamleader"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>指导教师：</td>
                        <td>
                        	<!-- <input type="text" id="counselor" name="counselor" class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span> -->
                            
                            <input class="easyui-combobox" id="counselor"
							name="counselor"
							data-options="
									url:'../user/selectUser?type=counselor',
									method:'get',
									valueField:'realname',
									textField:'realname',
									panelHeight:'auto'
							">
                            
                             <!-- <select name="type" class="easyui-combobox"
                            id="type" style="width: 154px;" editable="false"
                            panelHeight="auto" >
                                <option value="">请选择角色</option>
                                <option value="系统管理员">系统管理员</option>
                                <option value="学生">学生</option>
                                <option value="团总支负责人">团总支负责人</option>
                                <option value="专业教师">专业教师</option>
                        	</select> &nbsp; --><span style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>成员：</td>
                        <td colspan="4">
                        <!-- <input type="text" id="members" name="members"
                            class="easyui-textbox" data-options="multiline:true" required="true" />&nbsp;<span
                            style="color: red">*</span> -->
                            
                            <iframe src="user_team_management.jsp" width="1250px" height="900px"></iframe>
                        </td> 
                    </tr>
                    
                </table>
            </form>
        </div>
</body>
</html>