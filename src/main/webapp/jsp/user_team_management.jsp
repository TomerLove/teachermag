<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<title>学员大赛管理系统</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/demo/demo.css"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
    var url;
    function searchUser() {
        $("#dg").datagrid('load', {
            "contestname" : $("#s_userName").val()
        });
    }
    function openUserAddDialog() {
        $("#dlg").dialog("open").dialog("setTitle", "添加信息");
        $("#dlg").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5,left:$(document.body).width()/4}); 
        url = "${pageContext.request.contextPath}/userTeam/save";
    }

    function openUserModifyDialog() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length != 1) {
            $.messager.alert("系统提示", "请选择一条要编辑的数据！");
            return;
        }
        var row = selectedRows[0];
        $("#dlg").dialog("open").dialog("setTitle", "编辑信息");
        $("#dlg").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5,left:$(document.body).width()/4}); 
        $("#fm").form("load", row);
        url = "${pageContext.request.contextPath}/userTeam/save.do?id=" + row.id;
    }
    
    function saveUser() {
        $("#fm").form("submit", {
            url : url,
            onSubmit : function() {
                
                return $(this).form("validate");
            },
            success : function(result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
                } else {
                    $.messager.alert("系统提示", "保存失败！");
                    return;
                }
            }
        });
    }

    function resetValue() {
        $("#userid").val("");
        $("#teamno").val("");
        $("#userduty").val("");
    }

    function closeUserDialog() {
        $("#dlg").dialog("close");
        resetValue();
    }

    function deleteUser() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length == 0) {
            $.messager.alert("系统提示", "请选择要删除的数据！");
            return;
        }
        var strIds = [];
        for ( var i = 0; i < selectedRows.length; i++) {
            strIds.push(selectedRows[i].id);
        }
        var ids = strIds.join(",");
        $.messager.confirm("系统提示", "您确定要删除这<font color=red>"
                + selectedRows.length + "</font>条数据吗？", function(r) {
            if (r) {
                $.post("${pageContext.request.contextPath}/userTeam/delete.do", {
                    ids : ids
                }, function(result) {
                    if (result.success) {
                        $.messager.alert("系统提示", "数据已成功删除！");
                        $("#dg").datagrid("reload");
                    } else {
                        $.messager.alert("系统提示", "数据删除失败，请联系系统管理员！");
                    }
                }, "json");
            }
        });
    }
    function passwordFormatter(value,rowData,rowIndex){
		return '******';
	} 
</script>
</head>

<body style="margin: 1px">

    <table id="dg" title="成员管理" class="easyui-datagrid" fitColumns="true"
         rownumbers="true"
        url="../userTeam/listAll.do" fit="true"
        toolbar="#tb">
        <thead>
            <tr>
				<th field="id" checkbox="true" align="center"></th>
				<th data-options="field:'userid',editor:'textbox'">成员</th>
				<th data-options="field:'teamno',editor:'textbox'">团队</th>
				<th data-options="field:'userduty',editor:'textbox'">职责</th>
            </tr>
        </thead>
    </table>
    <div id="tb">
        <a href="javascript:openUserAddDialog()" class="easyui-linkbutton"
            iconCls="icon-add" plain="true">添加</a> <a
            href="javascript:openUserModifyDialog()" class="easyui-linkbutton"
            iconCls="icon-edit" plain="true">修改</a> <a
            href="javascript:deleteUser()" class="easyui-linkbutton"
            iconCls="icon-remove" plain="true">删除</a> 

        <div id="dlg-buttons">
            <a href="javascript:saveUser()" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
                class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>

        <div id="dlg" class="easyui-dialog"
            style="width: 740px;height:250px;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <form method="post" id="fm">
                <table cellspacing="8px;">
                    <tr>
                        <td>成员：</td>
                        <td><input type="text" id="userid" name="userid"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>团队：</td>
                        <td><input type="text" id="teamno" name="teamno"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>职责：</td>
                        <td><input type="text" id="userduty" name="userduty"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td></td>
                        <td> 
                        </td>
                    </tr>
                </table>
            </form>
        </div>
</body>
</html>