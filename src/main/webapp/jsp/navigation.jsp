<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="UTF-8">
<title>学员大赛管理系统</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery easyui/demo/demo.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery easyui/jquery.easyui.min.js"></script>
</head>
<body class="easyui-layout">  
<script type="text/javascript">  
        var index = 0;  
        var url;
        function addPanel(url,title){  
            if(!$('#tt').tabs('exists', title)){  
                $('#tt').tabs('add',{  
                    title: title,  
                    content: '<iframe src="'+url+'" frameBorder="0" border="0"  style="width: 100%; height: 100%;"/>',  
                    closable: true  
                });  
            }else{  
                $('#tabs').tabs('select', title);  
            }  
        }  
        function removePanel(){  
            var tab = $('#tt').tabs('getSelected');  
            if (tab){  
                var index = $('#tt').tabs('getTabIndex', tab);  
                $('#tt').tabs('close', index);  
            }  
        }    
        $(document).ready(function (){
        	var role = "${currentUser.type}";
        	var html = "<ul id='tt1' class='easyui-tree'>";
        	if("${currentUser.username}" == "superadmin"){
        		html += "<li>                                                                                                                                                                            ";
       			html += "	<span>系统管理</span>                                                                                                                                                        ";
       			html += "	<ul>                                                                                                                                                                         ";
       			html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/user_management.jsp','用户管理')\">用户管理</a></span></li>                                            ";
       			html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/contest_management.jsp','大赛基本信息管理')\">大赛基本信息管理</a></span></li>                         ";
       			html += "	</ul>                                                                                                                                                                        ";
       			html += "</li>                                                                                                                                                                           ";
       			html += "<li>                                                                                                                                                                            ";
       			html += "	<span>大赛通知管理</span>                                                                                                                                                    ";
       			html += "	<ul>                                                                                                                                                                         ";
       			html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/release_management.jsp','大赛通知管理')\">大赛通知管理</a></span></li>                                 ";
       			html += "	</ul>                                                                                                                                                                        ";
       			html += "</li>                                                                                                                                                                           ";
       			html += "<li>                                                                                                                                                                            ";
       			html += "	<span>组队管理</span>                                                                                                                                                        ";
       			html += "	<ul>                                                                                                                                                                         ";
       			html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/team_management.jsp','组队管理')\">组队管理</a></span></li>                                            ";
       			html += "	</ul>                                                                                                                                                                        ";
       			html += "</li>                                                                                                                                                                           ";
       			html += "<li>                                                                                                                                                                            ";
       			html += "	<span>获奖管理</span>                                                                                                                                                        ";
       			html += "	<ul>                                                                                                                                                                         ";
       			html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/honor_management.jsp','获奖管理')\">获奖管理</a></span></li>                                           ";
       			html += "	</ul>                                                                                                                                                                        ";
       			html += "</li>                                                                                                                                                                           ";
        		
        	}
        	if(role == "系统管理员"){
	        	html += "<li>                                                                                                                                                                ";
	        	html += "	<span>系统管理</span>                                                                                                                                            ";
	        	html += "	<ul>                                                                                                                                                             ";
	        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/user_management.jsp','用户管理')\">用户管理</a></span></li>                     ";
	        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/contest_management.jsp','大赛基本信息管理')\">大赛基本信息管理</a></span></li>            ";
	        	html += "	</ul>                                                                                                                                                            ";
	        	html += "</li>                                                                                                                                                               ";
	        }
        	if(role == "辅导员" || role == "学生" || role == "专业教师"){
	        	html += "<li>                                                                                                                                                                ";
	        	html += "	<span>大赛通知管理</span>                                                                                                                                        ";
	        	html += "	<ul>                                                                                                                                                             ";
	        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/release_management.jsp','大赛通知管理')\">大赛通知管理</a></span></li>                    ";
	        	html += "	</ul>                                                                                                                                                            ";
	        	html += "</li> ";
        	}                                                                                                                                                
        	if(role == "学生"){
	        	html += "<li>                                                                                                                                                                ";
	        	html += "	<span>组队管理</span>                                                                                                                                            ";
	        	html += "	<ul>                                                                                                                                                             ";
	        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/team_management.jsp','组队管理')\">组队管理</a></span></li>                               ";
	        	html += "	</ul>                                                                                                                                                            ";
	        	html += "</li>                                                                                                                                                               ";
	        }

        	if(role == "辅导员" || role == "学生" || role == "专业教师"){
	       		html += "<li>                                                                                                                                                                ";
	       		html += "	<span>获奖管理</span>                                                                                                                                            ";
	        	html += "	<ul>                                                                                                                                                             ";
	        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/honor_management.jsp','获奖管理')\">获奖管理</a></span></li>                              ";
	        	html += "	</ul>                                                                                                                                                            ";
	        	html += "</li>                                                                                                                                                               ";
        	}
        	html += "<li>                                                                                                                                                                ";
        	html += "	<span>各类查询</span>                                                                                                                                            ";
        	html += "	<ul>                                                                                                                                                             ";
        	html += "		<li><span><a href='javascript:void(0)' onClick=\"addPanel('../jsp/pie.jsp','各类查询')\">各类查询</a></span></li>                                                         ";
        	html += "	</ul>                                                                                                                                                            ";
        	html += "</li>                                                                                                                                                               ";
        	html += "</ul>  ";
        	
        	
			document.getElementById("tree").innerHTML = html;        	
        })
        
        
        function openUserAddDialog() {
        $("#dlg").dialog("open").dialog("setTitle", "添加信息");
    }

    
    function saveUser() {
        $("#fm").form("submit", {
            url : "${pageContext.request.contextPath}/user/updateUserPass?oldpass="+$('#oldpass')+"&newpass"+$('#newpass'),
            onSubmit : function() {
                
                return $(this).form("validate");
            },
            success : function(result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
                } else {
                    $.messager.alert("系统提示", "保存失败！");
                    return;
                }
            }
        });
    }

    function resetValue() {
        $("#oldpass").val("");
        $("#newpass").val("");
        $("#confirmpass").val("");
    }

    function closeUserDialog() {
        $("#dlg").dialog("close");
        resetValue();
    }

    </script>  
      
    <div data-options="region:'north',split:true" style="height:100px;"><h1><b><font color="red">学员大赛管理系统</font></b></h1>
    <table width="100%">
    	<tr>
        	<td align="left">
            	<p>
                <a>【管理员】</a>&nbsp;&nbsp;&nbsp;
                <a>当前登录用户:【${currentUser.realname}(${currentUser.username})】</a> &nbsp;&nbsp;&nbsp;
                <a href="javascript:openUserAddDialog()">【修改密码】</a>&nbsp;&nbsp;&nbsp;
                <a href="../index.jsp">【退出系统】</a>
              </p>
            </td>
        	<td align="right">
            	作者联系方式:xxx@xxx.com
            </td>
        </tr>
    </table>
      
</div>  
    <div data-options="region:'west',title:'菜单',split:true" style="width:180px;">  
      
    <div id="tree"></div>
      

          
    </div>  
    <div id="tt" data-options="region:'center',title:'内容页'"  class="easyui-tabs" style="padding:5px;background:#eee;">  
  
    <div title="欢迎" style="padding:20px;display:none;">  
	欢迎使用《学员大赛管理系统》<br>
	有任何疑问请联系xxx@xxx.com
    </div>  
    </div>  
        <div id="dlg-buttons">
            <a href="javascript:saveUser()" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
                class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>
      <div id="dlg" class="easyui-dialog"
            style="width: 730px;height:280px;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <form method="post" id="fm">
                <table cellspacing="8px;"> 
                    <tr>
                        <td>新密码：</td>
                        <td><input type="text" id="newpass" name="newpass"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr> 
                </table>
            </form>
        </div>
  </body>  
</html>  